package com.constellio.app.ui.pages.management.TemporaryRecord;

import com.constellio.app.modules.rm.ui.pages.viewGroups.PersonnalSpaceViewGroup;
import com.constellio.app.ui.pages.base.BaseView;

public interface ListTemporaryRecordView extends BaseView, PersonnalSpaceViewGroup {
}
